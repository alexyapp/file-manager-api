<?php

namespace Tests\Feature;

use App\Models\User;
use App\Jobs\ProcessFileUpload;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FileUploadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_logged_in_user_may_only_upload_jpg_or_mp4_files()
    {
        $user = factory(User::class)->create();

        $headers = [
            'Authorization' => 'Bearer '.\JWTAuth::fromUser($user),
        ];

        $this->json('POST', '/api/files', [
            'files' => [
                UploadedFile::fake()->image('test.mp3'),
            ],
        ], $headers)->assertStatus(422);

        $this->json('POST', '/api/files', [
            'files' => [
                UploadedFile::fake()->image('test.mp4'),
            ],
        ], $headers)->assertStatus(202);
    }

    /** @test */
    public function an_uploaded_file_will_be_stored_in_the_aws_s3_bucket()
    {
        $user = factory(User::class)->create();

        $file = UploadedFile::fake()->image('test.mp4');

        ProcessFileUpload::dispatchNow($user, $file);

        Storage::disk('s3')->assertExists("files/{$user->uuid}/{$file->hashName()}");
    }
}
