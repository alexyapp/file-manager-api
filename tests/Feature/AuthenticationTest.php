<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function users_may_register_for_an_account()
    {
        $user = factory(User::class)->make();

        $response = $this->json('POST', '/api/auth/register', array_merge(
            $user->only(['name', 'email']),
            [
                'password' => 'password',
                'password_confirmation' => 'password',
            ]
        ));

        // $this->assertSessionHasErrors([
        // ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('users', [
            'email' => $user->email,
        ]);

        Log::info('User created.', ['user' => $user->toArray()]);
    }

    /** @test */
    public function users_may_login_to_the_app()
    {
        $password = 'password';

        $user = factory(User::class)->create([
            'password' => $password,
        ]);

        $this->json('POST', '/api/auth/login', [
            'email' => $user->email,
            'password' => $password,
        ])->assertStatus(200)->assertJsonStructure([
            'access_token', 'token_type', 'expires_in',
        ]);
    }
}
