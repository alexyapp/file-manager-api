<?php

namespace App\Contracts;

interface HasFilesInterface
{
    /**
     * @return Illuminate\Support\Collection
     */
    public function files();
}
