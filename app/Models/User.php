<?php

namespace App\Models;

use App\Contracts\HasFilesInterface;
use App\Traits\HasFiles;
use App\Jobs\EncryptFile;
use App\Jobs\ProcessFileUpload;
use Webpatser\Uuid\Uuid;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\UploadedFile;

class User extends Authenticatable implements JWTSubject, HasFilesInterface
{
    use Notifiable, HasFiles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     *  Setup model event hooks.
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Set the user's password.
     *
     * @param string $value
     */
    public function setPasswordAttribute(string $value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Register a user.
     *
     * @param array $data
     *
     * @return \App\Models\User
     */
    public static function register(array $data)
    {
        return self::create($data);
    }

    /**
     * Upload a file.
     *
     * @param \Illuminate\Http\UploadedFile $file
     */
    public function upload(UploadedFile $file)
    {
        ProcessFileUpload::dispatch($this, $file);

        // $path = Storage::putFile('files/'.auth()->user()->uuid, $file);

        // if ($path) {
        //     // EncryptFile::withChain([
        //     //     new ProcessFileUpload($path),
        //     // ])->dispatch($path);
        // }
    }
}
