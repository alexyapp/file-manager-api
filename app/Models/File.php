<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'size', 'mime_type',
    ];

    /**
     * Get the owner of a file.
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
