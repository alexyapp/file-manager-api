<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Http\Resources\File as FileResource;
use App\Http\Requests\UploadRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if (filter_var(request('pending'), FILTER_VALIDATE_BOOLEAN)) {
            $localFiles = Storage::files('files/'.auth()->user()->uuid);

            $files = new Collection();

            // TODO: refactor
            foreach ($localFiles as $path) {
                if ($user->files->count()) {
                    $user->files->each(function ($file) use ($path, $files) {
                        if ($file->name !== basename($path)) {
                            $file = new File();
                            $file->name = basename($path);
                            $files->push($file);
                        }
                    });
                } else {
                    $file = new File();
                    $file->name = basename($path);
                    $files->push($file);
                }
            }

            $files = $files->unique();
        } else {
            $files = $user->files;
        }

        return FileResource::collection($files);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UploadRequest $request)
    {
        $user = auth()->user();

        if ($files = $request->file('files')) {
            foreach ($files as $file) {
                try {
                    $user->upload($file);
                } catch (\Exception $e) {
                    return response()->json([
                        'message' => $e->getMessage(),
                    ], 422);
                }
            }

            return response()->json([
                'message' => 'It looks like this might take a while. We\'ll notify you once uploading is done.',
            ], 202);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
