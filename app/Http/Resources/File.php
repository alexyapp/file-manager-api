<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class File extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'mime_type' => $this->mime_type,
            'size' => $this->size ? round($this->size / 1000) : null,
            'created_at' => optional($this->created_at)->format('F d, Y h:i A') ?: null,
        ];
    }
}
