<?php

namespace App\Jobs;

use SoareCostin\FileVault\Facades\FileVault;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EncryptFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The file to be encrypted's name.
     *
     * @var string
     */
    protected $filename;

    /**
     * Create a new job instance.
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        FileVault::encrypt($this->filename);
    }
}
