<?php

namespace App\Jobs;

use App\Notifications\UploadFinished;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\UploadedFile;

class ProcessFileUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 0;

    /**
     * The entity which will be associated with the file.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $entity;

    /**
     * The path to the file to be uploaded to Amazon S3.
     *
     * @var string
     */
    protected $path;

    /**
     * Create a new job instance.
     */
    public function __construct(Model $entity, UploadedFile $file)
    {
        $this->entity = $entity;
        $this->path = Storage::putFile('files/'.$entity->uuid, $file);
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Log::info('Uploading file in progress...');

        $path = Storage::disk('s3')->putFileAs(
            '/',
            new File(storage_path('app/'.$this->path)),
            $this->path,
            [
                'visibility' => 'public',
            ]
        );

        gc_collect_cycles();

        if ($path === false) {
            throw new Exception("Couldn't upload file to S3.");
        }

        $file = $this->entity->files()->create([
            'name' => basename($path),
            'url' => Storage::disk('s3')->url($path),
            'size' => Storage::disk('s3')->size($path),
            'mime_type' => Storage::disk('s3')->mimeType($path),
        ]);

        if (!Storage::disk('local')->delete($this->path)) {
            // throw new Exception('File could not be deleted from the local filesystem.');
        }

        $this->entity->notify(new UploadFinished($file));

        Log::info('Uploading successful.');
    }
}
