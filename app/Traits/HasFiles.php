<?php

namespace App\Traits;

use App\Models\File;

trait HasFiles
{
    /**
     * Retrieve all files associated with the entity.
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }
}
