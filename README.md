## API Setup
cd file-manager-api

### Hosts
For Mac users:
1. Run the following command on your terminal:
sudo nano /etc/hosts

2. Enter the following:
127.0.0.1 file-manager.test

3. Exit and save the file

For Windows users:
1. Open Notepad as Admin

2. Navigate to C:\Windows\System32\drivers\etc

3. Make sure to set All Files

4. Open the hosts file

5. Enter the following:
127.0.0.1 file-manager.test

6. Exit and save the file

### Dependencies
composer install

Note: Some dependencies require PHP 7.3

### Environment
cp .env.example .env

Note: Everything in the .env.example should shuffice. Typically this file should never contain any sensitive information. I just left it there for the purposes of this demo.

### Generate Laravel App Key
php artisan key:generate

### Create symlink to storage folder
php artisan storage:link

### Generate JWT secret
php artisan jwt:secret

### Start Docker containers, run migrations, and start queue listener
cd laradock

cp env-example .env

docker-compose up -d nginx mysql phpmyadmin workspace

docker-compose exec workspace bash

Note: Running the above command on Windows will require a TTY input device. The quickest solution to this is to run the command using the PowerShell terminal on Windows.

php artisan migrate

Note: Before running the above command, make sure to create a database with the name file_manager or if you prefer to use a different name, make sure to adjust the value for the DB_DATABASE variable in the .env file accordingly. A quick way to create a database is with the provided phpMyAdmin Docker container which can be accessed on http://localhost:8081. The default password for the root user is 'root' and enter 'mysql' for the Server field.

php artisan queue:work

Note: Make sure to not close the terminal at this point.

### Tests
cd laradock

docker-compose exec workspace bash

phpunit

### Nice to haves (if I had more time)
1. Websocket support
2. Customizable file names
3. Storage size limit for each account (like in Dropbox)
4. File deletion and pagination