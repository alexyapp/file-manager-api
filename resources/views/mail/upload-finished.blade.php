@component('mail::message')
# Upload Finished

Hi, {{ $user->name }}!

Your file {{ $file->name }} on GoReact File Manager has finished uploading.

@component('mail::button', ['url' => config('app.client_url') . '/dashboard'])
View File
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent