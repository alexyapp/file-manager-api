@component('mail::message')
# Account Created

Hi, {{ $user->name }}!

Your account on GoReact File Manager has been created.

@component('mail::button', ['url' => config('app.client_url')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent